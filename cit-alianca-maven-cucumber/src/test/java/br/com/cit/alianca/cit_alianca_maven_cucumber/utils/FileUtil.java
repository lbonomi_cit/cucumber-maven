package br.com.cit.alianca.cit_alianca_maven_cucumber.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class FileUtil {

	public static String getProperties(String property) {
        return getPropertiesFile().getProperty(property);
    }

    private static Properties getPropertiesFile() {
        Properties result = null;
        try {
            result = new Properties();
            result.load(new FileInputStream(Constants.PROPERTIES_PATH));
        } catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Erro ao carrefar o arquivo: " + Constants.PROPERTIES_PATH);
        }
        return result;
    }
}
