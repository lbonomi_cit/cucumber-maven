package br.com.cit.alianca.cit_alianca_maven_cucumber.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class OpenPage {

    protected WebDriver webDriver;

    @FindBy(id = "mosaico__wrapper")
    private WebElement selectTeam;

    @FindBy(xpath = "//*[@id='tab-content-1']/div[19]/a")
    private WebElement team;

    @FindBy(xpath = "//*[@id='header-produto']/div[2]/div/div/h1/div/a")
    private WebElement nameTeam;

	public OpenPage(WebDriver driver) {
        this.webDriver = driver;
        PageFactory.initElements(this.webDriver, this);
	}

	public void selectTeam() {
        this.selectTeam.click();
        this.team.click();
	}

	public String getTeamName() {
		return this.nameTeam.getText();
	}
}
