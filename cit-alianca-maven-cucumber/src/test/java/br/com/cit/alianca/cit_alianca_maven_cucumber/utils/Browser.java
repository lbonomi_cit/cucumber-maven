package br.com.cit.alianca.cit_alianca_maven_cucumber.utils;

public enum Browser {
	CHROME,
    IE,
    FIREFOX,
    SAFARI,
    EDGE,
    WINAPP,
    MACAPP
}
