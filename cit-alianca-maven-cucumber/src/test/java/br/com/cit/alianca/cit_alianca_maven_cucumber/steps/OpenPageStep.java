package br.com.cit.alianca.cit_alianca_maven_cucumber.steps;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import br.com.cit.alianca.cit_alianca_maven_cucumber.pages.OpenPage;
import br.com.cit.alianca.cit_alianca_maven_cucumber.utils.Browser;
import br.com.cit.alianca.cit_alianca_maven_cucumber.utils.FileUtil;
import br.com.cit.alianca.cit_alianca_maven_cucumber.utils.WebDriverFactory;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;

public class OpenPageStep {

	private WebDriver driver;

    @Dado("que estou na pagina da globoesporte")
    public void openBrowser() {
        driver = WebDriverFactory.creatWebDriver(Browser.CHROME);
        driver.get(FileUtil.getProperties("url"));
    }

    @Quando("seleciono o time sao paulo")
    public void selectTeam() {
        OpenPage openPage = new OpenPage(driver);
        openPage.selectTeam();
    }

    @Entao("sou redirecionado para a pagina do sao paulo no globoesporte")
    public void redirectedTeamPage() {
        OpenPage openPage = new OpenPage(driver);
        String teamName = openPage.getTeamName();
        Assert.assertEquals("SÃO PAULO", teamName);
    }
}