package br.com.cit.alianca.cit_alianca_maven_cucumber.utils;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class WebDriverFactory {

	public static WebDriver creatWebDriver(Browser browser) {

        WebDriver webDriver = null;

        switch(browser) {
            case CHROME:
                System.setProperty("webdriver.chrome.driver", FileUtil.getProperties("chromedriverpath"));

                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.addArguments("--incognito");
                chromeOptions.setExperimentalOption("useAutomationExtension", false);

                webDriver = new ChromeDriver(chromeOptions);
                webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
                webDriver.manage().window().maximize();

                return webDriver;

            default:
                throw new RuntimeException("Unsupported webdriver: " + browser);
        }
    }
}
